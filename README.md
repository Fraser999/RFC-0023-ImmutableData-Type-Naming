# Naming of `ImmutableData` Types

This is a sample implementation of the proposed changes to the naming of `ImmutableData` types, as described in RFC0023.

To run the benchmarks using the original proposal or with the [single-byte amendment](https://github.com/maidsafe/rfcs/issues/104#issuecomment-202421135):

    cargo run --release
    cargo run --release --features single-byte

There are also tests for the conversion functions:

    cargo test
    cargo test --features single-byte
