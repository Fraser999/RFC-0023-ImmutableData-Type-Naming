extern crate rand;
extern crate sodiumoxide;
extern crate time;
extern crate xor_name;

mod immutable_data;

use std::cmp;
use std::default::Default;
use std::fmt::{self, Debug, Formatter};
use immutable_data::{ImmutableData, ImmutableDataType};
use rand::{Rng, thread_rng};
use time::SteadyTime;
use xor_name::{XorName, XOR_NAME_LEN};

fn xor(lhs: &[u8; XOR_NAME_LEN], mut rhs: [u8; XOR_NAME_LEN]) -> XorName {
    for i in 0..XOR_NAME_LEN {
        rhs[i] ^= lhs[i];
    }
    XorName(rhs)
}

struct Values {
    pub normal: ImmutableData,
    pub backup: ImmutableData,
    pub sacrificial: ImmutableData,
}

impl Values {
    pub fn new(contents: Vec<u8>) -> Values {
        Values {
            normal: ImmutableData::new(ImmutableDataType::Normal, contents.clone()),
            backup: ImmutableData::new(ImmutableDataType::Backup, contents.clone()),
            sacrificial: ImmutableData::new(ImmutableDataType::Sacrificial, contents),
        }
    }
}

#[derive(Clone)]
struct Names {
    pub normal: XorName,
    pub backup: XorName,
    pub sacrificial: XorName,
}

impl Default for Names {
    fn default() -> Names {
        Names {
            normal: XorName::new([0u8; XOR_NAME_LEN]),
            backup: XorName::new([0u8; XOR_NAME_LEN]),
            sacrificial: XorName::new([0u8; XOR_NAME_LEN]),
        }
    }
}

impl Debug for Names {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "Normal: {}\nBackup: {}\nSacrificial: {}",
               self.normal,
               self.backup,
               self.sacrificial)
    }
}

struct Distances {
    pub n_to_b: XorName,
    pub n_to_s: XorName,
    pub b_to_s: XorName,
    pub smallest: XorName,
}

impl Distances {
    fn new(names: &Names) -> Distances {
        let n_to_b = xor(&names.normal.0, names.backup.0);
        let n_to_s = xor(&names.normal.0, names.sacrificial.0);
        let b_to_s = xor(&names.backup.0, names.sacrificial.0);
        let mut smallest = cmp::min(n_to_b, n_to_s);
        smallest = cmp::min(smallest, b_to_s);
        Distances {
            n_to_b: n_to_b,
            n_to_s: n_to_s,
            b_to_s: b_to_s,
            smallest: smallest,
        }
    }
}

impl Debug for Distances {
    fn fmt(&self, formatter: &mut Formatter) -> fmt::Result {
        write!(formatter, "N to B distance: {}\nN to S distance: {}\nB to S distance: {}\nSmallest distance: {}",
               self.n_to_b,
               self.n_to_s,
               self.b_to_s,
               self.smallest)
    }
}

fn format_distance(distance: XorName) -> String {
    format!("{:02x}{:02x}{:02x}{:02x}{:02x}...",
           distance.0[0],
           distance.0[1],
           distance.0[2],
           distance.0[3],
           distance.0[4])
}

fn main() {
    let count = 100_000;
    let mut values = Vec::with_capacity(count);
    for _ in 0..count {
        let contents = thread_rng().gen_iter().take(1000).collect::<Vec<u8>>();
        values.push(Values::new(contents));
    }

    let mut existing_names = vec![Names::default(); count];
    for i in 0..count {
        existing_names[i].normal = values[i].normal.name();
    }
    let mut start = SteadyTime::now();
    for i in 0..count {
        existing_names[i].backup = values[i].backup.name();
        existing_names[i].sacrificial = values[i].sacrificial.name();
    }
    let existing_microseconds = (SteadyTime::now() - start).num_microseconds().unwrap();
    println!("Time to process {} backup and sacrificial names with existing implementation: {}.{:06}s",
             count,
             existing_microseconds / 1_000_000,
             existing_microseconds % 1_000_000);

    let mut proposed_names = vec![Names::default(); count];
    for i in 0..count {
        proposed_names[i].normal = values[i].normal.proposed_name();
    }
    start = SteadyTime::now();
    for i in 0..count {
        proposed_names[i].backup = values[i].backup.proposed_name();
        proposed_names[i].sacrificial = values[i].sacrificial.proposed_name();
    }
    let proposed_microseconds = (SteadyTime::now() - start).num_microseconds().unwrap();
    println!("Time to process {} backup and sacrificial names with proposed implementation: {}.{:06}s",
             count,
             proposed_microseconds / 1_000_000,
             proposed_microseconds % 1_000_000);

    let diff = ((existing_microseconds - proposed_microseconds) as f64) / existing_microseconds as f64;
    println!("Proposed is {:.2}% faster than existing.", diff * 100f64);

    let mut exisiting_min_distance = XorName([255; XOR_NAME_LEN]);
    for name in &existing_names {
        let distances = Distances::new(name);
        exisiting_min_distance = cmp::min(exisiting_min_distance, distances.smallest);
    }

    let mut proposed_min_distance = XorName([255; XOR_NAME_LEN]);
    for name in &proposed_names {
        let distances = Distances::new(name);
        proposed_min_distance = cmp::min(proposed_min_distance, distances.smallest);
    }

    println!("Min distance for existing implementation: {}", format_distance(exisiting_min_distance));
    println!("Min distance for proposed implementation: {}", format_distance(proposed_min_distance));
}
