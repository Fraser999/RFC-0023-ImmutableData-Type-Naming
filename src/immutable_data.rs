#![allow(unused)]

use std::cmp;
use sodiumoxide::crypto::hash::sha512::{self, Digest};
use xor_name::{XorName, XOR_NAME_LEN};

#[cfg(feature = "single-byte")]
const NORMAL_TO_BACKUP: u8 = 128;
#[cfg(feature = "single-byte")]
const NORMAL_TO_SACRIFICIAL: u8 = 255;
#[cfg(feature = "single-byte")]
const BACKUP_TO_SACRIFICIAL: u8 = 127;

#[cfg(feature = "single-byte")]
fn xor(lhs: &[u8; XOR_NAME_LEN], rhs: u8) -> XorName {
    let mut result = *lhs;
    result[0] ^= rhs;
    XorName(result)
}

#[cfg(not(feature = "single-byte"))]
const NORMAL_TO_BACKUP: [u8; XOR_NAME_LEN] =
    [128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
     0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
#[cfg(not(feature = "single-byte"))]
const NORMAL_TO_SACRIFICIAL: [u8; XOR_NAME_LEN] = [255; XOR_NAME_LEN];
#[cfg(not(feature = "single-byte"))]
const BACKUP_TO_SACRIFICIAL: [u8; XOR_NAME_LEN] =
    [127, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255,
     255, 255, 255, 255];

#[cfg(not(feature = "single-byte"))]
fn xor(lhs: &[u8; XOR_NAME_LEN], mut rhs: [u8; XOR_NAME_LEN]) -> XorName {
    for i in 0..XOR_NAME_LEN {
        rhs[i] ^= lhs[i];
    }
    XorName(rhs)
}

pub enum ImmutableDataType {
    Normal,
    Backup,
    Sacrificial,
}

pub struct ImmutableData {
    type_tag: ImmutableDataType,
    value: Vec<u8>,
}

pub fn normal_to_backup(name: &XorName) -> XorName {
    xor(&name.0, NORMAL_TO_BACKUP)
}

pub fn backup_to_normal(name: &XorName) -> XorName {
    xor(&name.0, NORMAL_TO_BACKUP)
}

pub fn normal_to_sacrificial(name: &XorName) -> XorName {
    xor(&name.0, NORMAL_TO_SACRIFICIAL)
}

pub fn sacrificial_to_normal(name: &XorName) -> XorName {
    xor(&name.0, NORMAL_TO_SACRIFICIAL)
}

pub fn backup_to_sacrificial(name: &XorName) -> XorName {
    xor(&name.0, BACKUP_TO_SACRIFICIAL)
}

pub fn sacrificial_to_backup(name: &XorName) -> XorName {
    xor(&name.0, BACKUP_TO_SACRIFICIAL)
}

impl ImmutableData {
    pub fn new(type_tag: ImmutableDataType, value: Vec<u8>) -> ImmutableData {
        ImmutableData {
            type_tag: type_tag,
            value: value,
        }
    }

    pub fn name(&self) -> XorName {
        let digest = sha512::hash(&self.value);
        match self.type_tag {
            ImmutableDataType::Normal => XorName(digest.0),
            ImmutableDataType::Backup => XorName(sha512::hash(&digest.0).0),
            ImmutableDataType::Sacrificial => XorName(sha512::hash(&sha512::hash(&digest.0).0).0),
        }
    }

    pub fn proposed_name(&self) -> XorName {
        let digest = sha512::hash(&self.value);
        match self.type_tag {
            ImmutableDataType::Normal => XorName(digest.0),
            ImmutableDataType::Backup => xor(&digest.0, NORMAL_TO_BACKUP),
            ImmutableDataType::Sacrificial => xor(&digest.0, NORMAL_TO_SACRIFICIAL),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use xor_name::XorName;
    use rand;

    #[test]
    fn names() {
        let normal_name: XorName = rand::random();
        let backup_name = normal_to_backup(&normal_name);
        let sacrificial_name = normal_to_sacrificial(&normal_name);

        assert_eq!(normal_name, backup_to_normal(&backup_name));
        assert_eq!(normal_name, sacrificial_to_normal(&sacrificial_name));

        assert_eq!(sacrificial_name, backup_to_sacrificial(&backup_name));
        assert_eq!(backup_name, sacrificial_to_backup(&sacrificial_name));
    }
}
